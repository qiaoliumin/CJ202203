// app.js
const ald = require('./utils/ald-stat.js')
App({
    data: {
        user: "",
        system: "",
        ss: "",
        TG: ""

    },
    /**
     * 当小程序初始化完成时，会触发 onLaunch（全局只触发一次）
     */
    onLaunch: function (e) {
        if (wx.cloud) {
            wx.cloud.init({
                traceUser: true
            })
        }
        wx.cloud.callFunction({ //获取openid------------
            name: 'OP',
        }).then(res => {
            this.globalData.OP = res.result.openid
        })
        wx.getSystemInfo({
            success: e => {
                this.data.ss = e.system.indexOf("iOS");
            }
        })
    },

    /**
     * 当小程序启动，或从后台进入前台显示，会触发 onShow
     */
    onShow: function (options) {
        let that = this
        console.log("小程序开始运行了----------------app_onShow",)
        that.CP()
        that.globalData.query = options
        if (options.query.scene) {
            that.data.TG = options.query.scene
            that.globalData.scene = options.query.scene
        }
        wx.cloud.database() //普通获取数据------------
            .collection("system")
            .get()
            .then(res => {
                if (res.data.length == 1) {
                    console.log('获取system----------------app_onShow')
                    that.data.system = res.data[0]
                }
            })
        that.user()
    },
    globalData: {
        userInfo: [],
        list:"",//拼多多
        OP: ""
    },
    user() {
        let that = this
        wx.cloud.callFunction({ //获取openid------------
            name: 'api',
            data: {
                if: "user",
            }
        }).then(res => {
            if (res.result.data.length == 1) {
                console.log("用户信息----------------app_onShow")
                that.globalData.userInfo = {
                    avatarUrl: res.result.data[0].avatarUrl,
                    nickName: res.result.data[0].nickName,
                }
            }
            that.data.user = res.result.data[0]
        })
    },
    CP() {
        //  return
        // 在页面中定义插屏广告
        let interstitialAd = null
        // 在页面onLoad回调事件中创建插屏广告实例
        if (wx.createInterstitialAd) {
            interstitialAd = wx.createInterstitialAd({
                adUnitId: 'adunit-0ab6d6edc221fc08'
            })
            interstitialAd.onLoad(() => { })
            interstitialAd.onError((err) => { })
            interstitialAd.onClose(() => { })
        }

        // 在适合的场景显示插屏广告
        if (interstitialAd) {
            console.log("在适合的场景显示插屏广告----------------app_onShow")
            interstitialAd.show().catch((err) => {
                console.error(err)
            })
        }
    }
})
