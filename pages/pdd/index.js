const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    offset: 1, // 页数，爆款商品
    page: 1, // 页数, 搜索商品
    limit: 20,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      detail: app.globalData.list
    })
  },
  // 生成链接，跳转到拼多多
  topdd(r) {
    var id = this.data.detail.goods_sign;
    var searchid = this.data.detail.searchid;
    wx.showLoading({
      title: '正在兑换',
      mask: true
    })
    wx.cloud.callFunction({
      name: 'pdd',
      data: {
        type: 'pddurl',
        data: {
          goods_sign: id,
          searchid: searchid
        }
      },
      success(res) {
        console.log(res, "pddurl")
        var obj = JSON.parse(res.result);
        setTimeout(function () {
          wx.hideLoading()
        }, 200)
        var weinfo = obj.goods_promotion_url_generate_response.goods_promotion_url_list[0].we_app_info;
        wx.navigateToMiniProgram({
          appId: weinfo.app_id,
          path: weinfo.page_path,
          success(res) {
            console.log(res, "打开成功")
            // 打开成功
          },
          complete() {
            wx.hideLoading()
          }
        })
      }
    })
  },
  XZ(e) {
    console.log(e.currentTarget.dataset.index)
    this.setData({
      detail: this.data.pddList[e.currentTarget.dataset.index]
    })
    app.globalData.list = this.data.pddList[e.currentTarget.dataset.index]
    this.goTop()
  },
  // 获取滚动条当前位置
  onPageScroll: function (e) {
    let isTop = e.scrollTop > 100;
    this.setData({
      floorstatus: isTop ? true : false
    });
  },

  // 更多，翻页
  more() {
    var that = this;
    var keyword = that.data.keyword || '';
    if (keyword != '') {
      // 有关键词，则搜索翻页
      var page = that.data.page + 1;
      that.setData({
        page: page
      })
      that.pddsearch()
    } else {
      // 爆款翻页
      var offset = that.data.offset + 1;
      that.setData({
        offset: offset
      })
      that.pddhot()
    }
  },
  //回到顶部
  goTop: function (e) { // 一键回到顶部
    if (wx.pageScrollTo) {
      wx.pageScrollTo({
        scrollTop: 0
      })

    } else {
      wx.showModal({
        title: '提示',
        content: '当前微信版本过低，无法使用该功能，请升级到最新微信版本后重试。'
      })
    }
  },
  // 拼多多爆款商品
  pddhot() {
    var that = this;
    wx.showLoading({
      title: '加载中...',
      mask: true
    })
    wx.cloud.callFunction({
      name: 'pdd',
      data: {
        type: 'pddhot',
        data: {
          list_id: that.data.list_id || '',
          offset: that.data.offset * that.data.limit,
          limit: that.data.limit,
        }
      },
      success(res) {
        var obj = JSON.parse(res.result);
        console.log(obj, "pddhot")
        var pddhotList = that.data.pddhotList || [];
        pddhotList = pddhotList.concat(obj.goods_basic_detail_response.list)
        that.setData({
          pddhotList: pddhotList,
          list_id: obj.goods_basic_detail_response.search_id,
          pddList: pddhotList
        })
      },
      complete() {
        wx.hideLoading()
      }
    })
  },
  /**
 * 页面上拉触底事件的处理函数
 */
  onReachBottom: function () {
    this.more()
  },
  ANNN() {
    wx.showShareMenu({
      withShareTicket: true,
      menus: ["shareAppMessage", "shareTimeline"]
    })
  },
  onShareAppMessage() {
    return {
      title: this.data.detail.goods_desc,
      imageUrl: this.data.detail.goods_image_url,
    };
  },
  onShareTimeline() {
    return {
      title: this.data.detail.goods_desc,
      imageUrl: this.data.detail.goods_image_url,
    };
  }
})