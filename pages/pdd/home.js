const app = getApp();
Component({
  /**
   * 组件的属性列表
   */
  properties: {

  },

  /**
   * 组件的初始数据
   */
  data: {
    iconList: [{
      appid: 'wx91d27dbf599dff74',
      path: 'pages/h5/index?__pid=Pxiybdszree1',
      url: "https://ss1.bdstatic.com/70cFvXSh_Q1YnxGkpoWK1HF6hhy/it/u=332832194,2527070225&fm=26&gp=0.jpg",
      name: '小程序'
    }],
    gridCol: 3,
    skin: false,
    offset: 1, // 页数，爆款商品
    page: 1, // 页数, 搜索商品
    limit: 40
  },

  /**
   * 组件的方法列表
   */
  methods: {
    TZ(e) {
      console.log(e.currentTarget.dataset.index)
      app.globalData.list = this.data.pddList[e.currentTarget.dataset.index]
      app.globalData.pddList = this.data.pddList
      wx.navigateTo({
        url: './index',
      })
    },
    // 拼多多爆款商品
    pddhot() {
      var that = this;
      wx.showLoading({
        title: '加载中...',
        mask: true
      })
      wx.cloud.callFunction({
        name: 'pdd',
        data: {
          type: 'pddhot',
          data: {
            list_id: that.data.list_id || '',
            offset: that.data.offset * that.data.limit,
            limit: that.data.limit,
          }
        },
        success(res) {
          var obj = JSON.parse(res.result);
          console.log(obj, "pddhot")
          var pddhotList = that.data.pddhotList || [];
          pddhotList = pddhotList.concat(obj.goods_basic_detail_response.list)
          that.setData({
            pddhotList: pddhotList,
            list_id: obj.goods_basic_detail_response.search_id,
            pddList: pddhotList
          })
        },
        complete() {
          wx.hideLoading()
        }
      })
    },
    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {
      this.more()
    },
    // 生成链接，跳转到拼多多
    topdd(re) {
      var id = re.currentTarget.dataset.id;
      var searchid = re.currentTarget.dataset.searchid;
      wx.showLoading({
        title: '正在生成链接',
        mask: true
      })
      wx.cloud.callFunction({
        name: 'pdd',
        data: {
          type: 'pddurl',
          data: {
            goods_sign: id,
            searchid: searchid
          }
        },
        success(res) {
          console.log(res, "pddurl")
          var obj = JSON.parse(res.result);
          var weinfo = obj.goods_promotion_url_generate_response.goods_promotion_url_list[0].we_app_info;
          wx.navigateToMiniProgram({
            appId: weinfo.app_id,
            path: weinfo.page_path,
            success(res) {
              console.log(res, "打开成功")
              // 打开成功
            },
            complete() {
              wx.hideLoading()
            }
          })
        }
      })
    },
    // 搜索关键字
    keyword: function (e) {
      var that = this;
      var keyword = e.detail.value;
      console.log(keyword)
      if (keyword != '') {
        that.setData({
          keyword: keyword
        })
        that.pddsearch()
      } else {
        that.setData({
          keyword: '',
          pddList: that.data.pddhotList
        })
      }
    },
    // 拼多多，搜索商品
    pddsearch() {
      var that = this;
      wx.showLoading({
        title: '加载中...',
        mask: true
      })
      wx.cloud.callFunction({
        name: 'pdd',
        data: {
          type: 'pddsearch',
          data: {
            keyword: that.data.keyword,
            page: "1",
            page_size: that.data.limit
          }
        },
        success(res) {
          console.log(res, "pddsearch")
          var obj = JSON.parse(res.result);
          console.log(obj, "CXobj")
          var pddsearchList = that.data.pddsearchList || [];
          pddsearchList = pddsearchList.concat(obj.goods_search_response.goods_list)
          that.setData({
            pddsearchList: obj.goods_search_response.goods_list,
            pddList: obj.goods_search_response.goods_list
          })
        },
        complete() {
          wx.hideLoading()
        }
      })

    },

    // 更多，翻页
    more() {
      var that = this;
      var keyword = that.data.keyword || '';
      if (keyword != '') {
        // 有关键词，则搜索翻页
        var page = that.data.page + 1;
        that.setData({
          page: page
        })
        that.pddsearch()
      } else {
        // 爆款翻页
        var offset = that.data.offset + 1;
        that.setData({
          offset: offset
        })
        that.pddhot()
      }
    },
    // 获取滚动条当前位置
    onPageScroll: function (e) {
      let isTop = e.scrollTop > 100;
      this.setData({
        floorstatus: isTop ? true : false
      });
    },
    //回到顶部
    goTop: function (e) { // 一键回到顶部
      if (wx.pageScrollTo) {
        wx.pageScrollTo({
          scrollTop: 0
        })
      } else {
        wx.showModal({
          title: '提示',
          content: '当前微信版本过低，无法使用该功能，请升级到最新微信版本后重试。'
        })
      }
    },
  },
  lifetimes: {
    attached: function () {
      var that = this;
      that.pddhot();
      that.setData({
        configObj: getApp().configObj
      })
    }
  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})