const app = getApp();
const db = wx.cloud.database()
const _ = db.command
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options)
    if (options.op=='o5Gpt5ATFAyRIhjL207-qGw2EG3Q') {
      this.SJ()
      this.FXT_LIST()
    }

  },
  SJ() {
    wx.cloud.database() //普通获取数据------------
      .collection("system")
      .get()
      .then(res => {
        console.log('获取成功', res)
        this.setData({
          list: res.data[0]
        })
        wx.hideLoading()
      })
  },
  FXT_LIST() {
    wx.cloud.database() //普通获取数据------------
      .collection("list")
      .get()
      .then(res => {
        console.log('获取成功', res)
        this.setData({
          FXT_LIST: res.data
        })
        wx.hideLoading()
      })
  },
  ZFXY(e) {
    let that = this
    console.log(e.detail.value)
    wx.showLoading({
      title: '支付开关修改中',
    })
    setTimeout(function () {
      wx.hideLoading()
    }, 2000)
    db.collection('system').doc(that.data.list._id).update({
      data: {
        ZFXY: e.detail.value
      },
      success: function (res) {
        if (res.stats.updated == 1) {
          wx.hideLoading()
          wx.showToast({
            title: '修改成功！',
          })
          that.SJ()
        }
      }
    })
  },
  TYXY(e) {
    let that = this
    console.log(e.detail.value)
    wx.showLoading({
      title: '体验小程序修改中',
    })
    setTimeout(function () {
      wx.hideLoading()
    }, 2000)
    db.collection('system').doc(that.data.list._id).update({
      data: {
        TYXY: e.detail.value
      },
      success: function (res) {
        if (res.stats.updated == 1) {
          wx.hideLoading()
          wx.showToast({
            title: '修改成功！',
          })
          that.SJ()
        }
      }
    })
  },
  JLXY(e) {
    let that = this
    console.log(e.detail.value)
    wx.showLoading({
      title: '激励修改中',
    })
    setTimeout(function () {
      wx.hideLoading()
    }, 2000)
    db.collection('system').doc(that.data.list._id).update({
      data: {
        JLXY: e.detail.value
      },
      success: function (res) {
        if (res.stats.updated == 1) {
          wx.hideLoading()
          wx.showToast({
            title: '修改成功！',
          })
          that.SJ()
        }
      }
    })
  },



  SH(e) {
    let that = this
    console.log(e)
    wx.showLoading({
      title: '上线修改中',
    })
    setTimeout(function () {
      wx.hideLoading()
    }, 2000)
    db.collection('list').doc(e.currentTarget.dataset.item._id).update({
      data: {
        SH: e.detail.value
      },
      success: function (res) {
        if (res.stats.updated == 1) {
          wx.hideLoading()
          wx.showToast({
            title: '修改成功！',
          })
          that.FXT_LIST()
        }
      }
    })
  },
  swiper(e) {
    let that = this
    console.log(e.detail.value)
    wx.showLoading({
      title: '轮播图修改中',
    })
    setTimeout(function () {
      wx.hideLoading()
    }, 2000)
    db.collection('list').doc(e.currentTarget.dataset.item._id).update({
      data: {
        swiper: e.detail.value
      },
      success: function (res) {
        if (res.stats.updated == 1) {
          wx.hideLoading()
          wx.showToast({
            title: '修改成功！',
          })
          that.FXT_LIST()
        }
      }
    })
  },
  ZZ(e) {
    let that = this
    console.log(e.detail.value)
    wx.showLoading({
      title: '抽奖页面修改中',
    })
    setTimeout(function () {
      wx.hideLoading()
    }, 2000)
    db.collection('list').doc(e.currentTarget.dataset.item._id).update({
      data: {
        ZZ: e.detail.value
      },
      success: function (res) {
        if (res.stats.updated == 1) {
          wx.hideLoading()
          wx.showToast({
            title: '修改成功！',
          })
          that.FXT_LIST()
        }
      }
    })
  }
})