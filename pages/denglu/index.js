const app = getApp();
Page({

    /**
     * 页面的初始数据
     */
    data: {

    },
    DL() {
        let that = this
        wx.getUserProfile({
            desc: '用于完善会员资料',
            success: (res) => {
                console.log(res)
                that.setData({
                    nickName: res.userInfo.nickName,
                    avatarUrl: res.userInfo.avatarUrl,
                    adds: res.userInfo.province + res.userInfo.city
                })
                if (app.data.TG) {
                    wx.cloud.callFunction({ //获取openid------------
                        name: 'api',
                        data: {
                            if: "TGADD",
                            tg_openid: app.data.TG,
                            XYB: 10,
                            HY_XY: true
                        }
                    }).then(res => {
                        console.log("res", res)
                    })
                }
                wx.cloud.callFunction({ //获取openid------------
                    name: 'api',
                    data: {
                        if: "add_user",
                        adds: that.data.adds,
                        nickName: that.data.nickName,
                        avatarUrl: that.data.avatarUrl,
                        tg_openid: app.data.TG
                    }
                }).then(res => {
                    console.log("res", res)
                    wx.navigateBack(1)
                    let item = {
                        nickName: that.data.nickName,
                        avatarUrl: that.data.avatarUrl,
                        _id: res.result._id,
                        TGnum: 0
                    }
                    app.data.user = item
                })
            }
        })

    },
    NoGetUserInfo() {
        wx.navigateBack(1)
    }
})