const app = getApp();
const db = wx.cloud.database()
const _ = db.command
var util = require('../../utils/util.js');
Page({

    /**
     * 页面的初始数据
     */
    data: {
        numList: [{
            name: 15
        }, {
            name: 20
        }, {
            name: 20
        }, {
            name: 40
        },
        {
            name: 25
        },
        {
            name: 25
        },
        {
            name: 100
        },


        ],
        num: 0,
        scroll: 0,
        iconList: [{
            icon: 'squarecheck',
            url: "../Sign/index",
            badge: 0.3,
            b_sum: 150
        }, {
            icon: 'goods',
            badge: 1.6,
            b_sum: 3000
        }, {
            url: "../turntable/index",
            icon: 'focus',
            badge: 8,
            b_sum: 12000
        }, {
            icon: 'present',
            badge: 18,
            b_sum: 30000
        }],
        gridCol: 4,
        skin: false
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        this.setData({
            web: app.data.system.web
        })
    },
    onShow: function () {
        this.user()
    },
    user() {
        let that = this
        wx.cloud.callFunction({ //获取openid------------
            name: 'api',
            data: {
                if: "user",
            }
        }).then(res => {
            wx.hideLoading()
            if (res.result.data.length == 1) {
                console.log("用户信息-----Sign")
                that.setData({
                    user: res.result.data[0],
                    QD_XY: res.result.data[0].QD_XY,
                    num: res.result.data[0].QD_sum,
                })
                app.data.user = res.result.data[0]
            }
        })
    },
    qdcg() {
        let that = this
        wx.requestSubscribeMessage({
            tmplIds: [
                '7fHKPTQr__G2cz9HWozZtGLqt-8d-4ABjyTJ-4PL8vw'
            ],
            success(res) {
                if (that.data.QD_XY) {
                    wx.showToast({
                        title: '已签到,继续努力',
                        icon: 'none',
                        duration: 2000
                    })
                    return
                }
                wx.showLoading({
                    title: '签到中',
                })
                wx.cloud.callFunction({ //获取openid------------
                    name: 'api',
                    data: {
                        if: "QD",
                        _id: that.data.user._id,
                        XYB: that.data.numList[that.data.num].name
                    }
                }).then(res => {
                    console.log(res)
                    if (that.data.num + 1 == 8) {
                        wx.cloud.callFunction({ //获取openid------------
                            name: 'api',
                            data: {
                                if: "QD_0",
                                _id: that.data.user._id
                            }
                        }).then(res => {
                            console.log(res)
                            if (res.result.stats.updated == 1) {
                                wx.showToast({
                                    title: '签到成功',
                                })
                                that.user()
                            }
                        })
                        return
                    }
                    if (res.result.stats.updated == 1) {
                        wx.showToast({
                            title: '签到成功',
                        })
                        that.user()
                    }


                })
            },
        })

    },

    Sign: function (e) {
        wx.switchTab({
            url: '../index/index',
        })
    },
    FFJL(e) {
        let that = this
        let list = that.data.iconList[e.currentTarget.dataset.index]
        let user = that.data.user
        let openid = user._id
        if (user.XYB < list.b_sum) {
            wx.showToast({
                title: "幸运币不足" + list.b_sum + '个',
                icon: 'none',
                duration: 2000
            })
            console.log("幸运币小于" + list.b_sum)
            return
        }
        wx.showLoading({
            title: '发放中...',
        })
        wx.cloud.callFunction({
            name: 'lq',
            data: {
                type: "transfers",
                openid: openid,
                appid: "wxcba5adf71bda7584",///appid
                partner_trade_no: util._id(new Date()),
                amount: list.badge * 100,///单位：分
                desc: '分享推小程序 ' + list.b_sum + '幸运币兑换红包'//描述
            }
        }).then(res => {
            console.log(res)
            if (res.result.result_code == 'SUCCESS') {
                wx.showToast({
                    title: '发放成功',
                    icon: 'success',
                    duration: 2000
                })
                setTimeout(function () {
                    wx.hideLoading()
                }, 200)
                db.collection('user').doc(openid).update({
                    data: {
                        // 表示指示数据库将字段自增 10
                        XYB: _.inc(-list.b_sum)
                    },
                    success: function (res) {
                        if (res.stats.updated == 1) {
                            that.user()
                        }
                    }
                })
            }
            else {
                wx.showToast({
                    title: '发放失败！',
                    icon: 'success',
                    duration: 2000
                })
                setTimeout(function () {
                    wx.hideLoading()
                }, 200)
            }
        }).catch(err => {
            console.log(err)
        })
    },
    JL() {
        wx.showLoading({
            title: '加载中',
        })
        let that = this
        if (that.data.user.JLGG_XY == false) {
            setTimeout(function () {
                wx.hideLoading()
            }, 2000)
            // 在页面中定义激励视频广告
            let videoAd = null
            // 在页面onLoad回调事件中创建激励视频广告实例
            if (wx.createRewardedVideoAd) {
                videoAd = wx.createRewardedVideoAd({
                    adUnitId: 'adunit-8461becb4e7527d3'
                })
                videoAd.onClose((status) => {
                    console.log("status")
                    if (status && status.isEnded || status === undefined) {
                        console.log("正常播放")
                        db.collection('user').doc(that.data.user._id).update({
                            data: {
                                // 表示指示数据库将字段自增 10
                                XYB: _.inc(20),
                                JLGG_XY: true
                            },
                            success: function (res) {
                                if (res.stats.updated == 1) {
                                    wx.showToast({
                                        title: '观看成功！+20幸运币',
                                    })
                                    that.user()
                                }
                            }
                        })
                    } else {
                        // 播放中途退出，进行提示
                        console.log("中途退出")
                        wx.showToast({
                            icon: 'none',
                            title: '看完广告参与抽奖呢。',
                        })
                    }
                })
                videoAd.onError((err) => {
                    console.log("激励视频加载失败")
                })
            }

            // 用户触发广告后，显示激励视频广告
            if (videoAd) {
                videoAd.show().catch(() => {
                    // 失败重试
                    videoAd.load()
                        .then(() => videoAd.show())
                        .catch(err => {
                            console.log('激励视频 广告显示失败')
                        })
                })
            }
        }
    },
    dz() {
        let that = this
        wx.chooseAddress({
            success(res) {
                db.collection('user').doc(that.data.user._id).update({
                    data: {
                        // 表示指示数据库将字段自增 10
                        adds: res.userName + '︱' + res.provinceName + res.cityName + res.countyName + res.detailInfo + '︱' + res.telNumber
                    },
                    success: function (res) {
                        if (res.stats.updated == 1) {
                            wx.showToast({
                                title: '更新成功',
                                icon: 'success',
                                duration: 6000
                            })
                            db.collection('user').doc(that.data.user._id).update({
                                data: {
                                    // 表示指示数据库将字段自增 10
                                    XYB: _.inc(20),
                                    dz_XY: true
                                },
                                success: function (res) {
                                    if (res.stats.updated == 1) {
                                        that.onShow()
                                        wx.showToast({
                                            title: '更新成功 +20幸运币',
                                        })
                                        that.user()
                                    }
                                }
                            })

                        }
                    }
                })
            }
        })
    },
    ANNN() {
        wx.showShareMenu({
            withShareTicket: true,
            menus: ["shareAppMessage", "shareTimeline"]
        })
    },
    onShareAppMessage() {
        return {
            title: app.data.system.fx_text,
            imageUrl: app.data.system.fx_img,
            path: '/pages/index/index?scene=' + app.globalData.OP
        };
    },
    onShareTimeline() {
        return {
            title: app.data.system.fx_text,
            imageUrl: app.data.system.fx_img,
            path: '/pages/index/index?scene=' + app.globalData.OP
        };
    }
})