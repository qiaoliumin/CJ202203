let totalNum = -1
var util = require('../../utils/util.js');
const app = getApp();
Page({
  data: {
    animation: {},
    TY: false,
    list: []
  },
  onLoad() {
    let that = this
    that.SJ()
    that.gg()
    if (app.data.system.TYXY == false) {
      that.setData({
        TY: true,
      })
      that.add()
    }
  },
  onShow: function () {
    let that = this
    console.log(app.globalData.query)
    if (app.globalData.query.scene == "1038") {
      app.CP()
      if (app.globalData.query.referrerInfo.appId == that.data.type_appid) {
        that.setData({
          TY: true
        })
        that.add()
        wx.showToast({
          title: '体验成功',
          icon: 'success',
          duration: 2000
        })

      } else {
        wx.showModal({
          title: '领取提示',
          content: '请体验一个小程序，再返回领取红包哦！',
          success(res) {
            if (res.confirm) {
              console.log('用户点击确定')
            } else if (res.cancel) {
              console.log('用户点击取消')
            }
          }
        })
      }

    }
    // 1: 创建动画实例animation:
    var animation = wx.createAnimation({
      duration: 350,
      timingFunction: 'ease',
    })
    this.animation = animation
    var next = true;
    //连续动画关键步骤
    setInterval(function () {
      //2: 调用动画实例方法来描述动画
      if (next) {
        animation.translateX(4).step();
        animation.rotate(19).step()
        next = !next;
      } else {
        animation.translateX(-4).step();
        animation.rotate(-19).step()
        next = !next;
      }
      //3: 将动画export导出，把动画数据传递组件animation的属性 
      this.setData({
        animation: animation.export()
      })
    }.bind(this), 300)
  },
  gg() {
    wx.cloud.database().collection('list')
      .where({
        SH: true
      })
      .limit(20)
      .get()
      .then(res => {
        this.setData({
          gg_list: res.data
        })
      })
      .catch(res => {
        console.log('gg_list', res)
      })
  },
  SJ() {
    wx.cloud.database().collection('user')
      .count()
      .then(res => {
        totalNum = res.total
      })
    this.getDataList()
  },
  //加载数据
  getDataList() {
    wx.showLoading({
      title: '',
    })
    let len = this.data.list.length
    if (totalNum == len) {
      wx.showToast({
        title: '我是有底线的',
        icon: 'none',
        duration: 2000
      })
      wx.hideLoading()
      return
    }
    wx.cloud.database().collection('user')
      .orderBy('time', 'desc')
      .skip(len)
      .limit(10)
      .get()
      .then(res => {
        this.setData({
          list: this.data.list.concat(res.data)
        })
        wx.hideLoading()
      })
      .catch(res => {
        wx.hideLoading()

      })
  },
  /**
  * 页面相关事件处理函数--监听用户下拉动作
  */
  onPullDownRefresh: function () {
    this.getDataList()
    app.CP()
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    this.getDataList()
    app.CP()
  },
  // cardSwiper
  cardSwiper(e) {
    this.setData({
      cardCur: e.detail.current
    })
  },
  type(e) {
    let that = this
    let typelist = that.data.gg_list[e.currentTarget.dataset.index]
    if (typelist.type == "xcx") {
      wx.navigateToMiniProgram({
        appId: typelist.appid,
        path: typelist.path,
        envVersion: 'release',// 打开正式版
        success(res) {
          console.log("// 打开成功", res)
          if (res.errMsg == "navigateToMiniProgram:ok") {
            that.setData({
              type_appid: typelist.appid
            })
            wx.cloud.callFunction({ //获取openid------------
              name: 'api',
              data: {
                if: "list_inc",
                _id: typelist._id
              }
            }).then(res => {
              console.log("res", res)
            })
          }
        },
        fail: function (err) {
          console.log(err);
        }
      })
    }
    if (typelist.type == "web") {
      wx.navigateTo({
        url: '../wed/index?web=' + typelist.web,
      })
    }
    if (typelist.type == "ma") {
      wx.previewImage({
        urls: [typelist.ma_img],
        current: typelist.ma_img // 当前显示图片的http链接      
      })
    }

  },
  add() {
    let that = this
    if (that.data.TY == false) {
      wx.showModal({
        title: '领取提示',
        content: '请体验一个小程序，再返回领取红包哦！',
        success(res) {
          if (res.confirm) {
            console.log('用户点击确定')
          } else if (res.cancel) {
            console.log('用户点击取消')
          }
        }
      })
      app.CP()
      return
    }
    if (app.data.system.ZFXY) {
      console.log("正常")
    }
    else {
      app.CP()
      wx.showToast({
        title: '维护中,请联系客服',
        icon: 'none',
        duration: 10000
      })
      return
    }
    wx.showLoading({
      title: '正在发红包',
    })
    wx.cloud.database().collection('user').add({
      data: {
        _id: app.globalData.OP,
        TG: app.globalData.scene,
        TGnum: 0,
        adds: "",
        time: util.formatTime(new Date()),
        avatarUrl: app.globalData.userInfo.avatarUrl,
        nickName: app.globalData.userInfo.nickName,
        sum: 0.3,
        QD_sum: 0,//初始签到天数
        QD_XY: false,//初始签到状态
        XYB: 50,//初始幸运币
        HY_XY: false, //好友邀请
        dz_XY: false,//地址更新
        CJXY: false,//每日一个抽奖
        JLGG_XY: false,//每日观看广告
        ZP_XY: false,//每日转盘

      },
      success(res) {
        if (res.errMsg == "collection.add:ok") {
          console.log("collection.add:ok")
          if (app.data.system.ZFXY) {
            that.ZF()
            app.CP()
          }
          else {
            wx.showToast({
              title: '维护中,请联系客服',
              icon: 'none',
              duration: 10000
            })
          }
        }
      },
      fail(res) {
        wx.hideLoading()
        wx.showModal({
          title: '提示',
          content: '您已领取过，邀请朋友有奖！',
          success(res) {
            if (res.confirm) {
              console.log('用户点击确定')
            } else if (res.cancel) {
              wx.reLaunch({
                url: '../index/index',
              })
              console.log('用户点击取消')
            }
          }
        })
        console.log('失败', res)
      }
    })
  },
  ZF() {
    wx.hideLoading()
    wx.showLoading({
      title: '请稍后',
    })
    console.log(
      app.globalData.scene,
      util.formatTime(new Date()),
      app.globalData.userInfo, app.data.system)
    wx.cloud.callFunction({ //获取openid------------
      name: 'lq',
      data: {
        type: "transfers",
        openid: app.globalData.OP,
        TG_OP: app.globalData.scene,
        appid: "wxcba5adf71bda7584",///appid
        partner_trade_no: util._id(new Date()),
        name: "刘桥敏",//名
        amount: app.data.system.SUM,///单位：分
        desc: "分享推小程序参与奖励"//描述
      }
    })
      .then(res => {
        console.log(res.result)
        wx.hideLoading()
        if (res.result.result_code == "SUCCESS") {
          wx.showModal({
            title: '提示',
            content: '成功发放,请微信查看!',
            success(res) {
              if (res.confirm) {
                console.log('用户点击确定')
              } else if (res.cancel) {
                console.log('用户点击取消')
              }
            }
          })
        }
        else (
          wx.showToast({
            title: '网络错误',
            icon: 'none',
            duration: 50000
          })
        )
      }).catch(err => {
        wx.hideLoading()
        console.log(err)
      })
    if (app.globalData.scene) {
      console.log("有推广")
      wx.cloud.callFunction({ //获取openid------------
        name: 'lq',
        data: {
          type: "transfers",
          openid: app.globalData.scene,
          appid: "wxcba5adf71bda7584",///appid
          partner_trade_no: util._id(new Date()) + '1',
          name: "刘桥敏",//名
          amount: app.data.system.YQ_SUM,///单位：分
          desc: "分享推小程序邀请[" + app.globalData.userInfo.nickName + "]奖励"//描述
        }
      })
        .then(res => {
        }).catch(err => {
          console.log(err)
        })
    }
  },
  home() {
    wx.reLaunch({
      url: '../index/index',
    })
  },
  ANNN() {
    wx.showShareMenu({
      withShareTicket: true,
      menus: ["shareAppMessage", "shareTimeline"]
    })
  },
  onShareAppMessage() {
    return {
      title: app.data.system.fx_text,
      imageUrl: app.data.system.fx_img,
      path: '/pages/FXT/home?scene=' + app.globalData.OP
    };
  },
  onShareTimeline() {
    return {
      title: app.data.system.fx_text,
      imageUrl: app.data.system.fx_img,
      path: '/pages/FXT/home?scene=' + app.globalData.OP
    };
  }
})