const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    navigateTo: true
  },
  onShow() {

  },
  goRuselt: function (options) {
    let that = this
    app.CP()
    if (app.data.user) {
      if (that.data.navigateTo) {
        that.setData({
          navigateTo: false
        })
        wx.navigateTo({
          url: './index',
        })
      } else {
        wx.switchTab({
          url: '../index/index',
        })
      }
    } else {
      wx.getUserProfile({
        desc: '用于展示头像呢陈',
        success: (res) => {
          app.globalData.userInfo = res.userInfo
          console.log(that.data.navigateTo)
          if (that.data.navigateTo) {
            that.setData({
              navigateTo: false
            })
            wx.navigateTo({
              url: './index',
            })
          } else {
            wx.switchTab({
              url: '../index/index',
            })
          }
        }
      })
    }

  },
 
})