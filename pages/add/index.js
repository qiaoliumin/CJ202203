const app = getApp();
var util = require('../../utils/util.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    KJnum: "",
    path: "",
    appid: "",
    name: "",
    src:""
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(app.globalData.list)
    this.setData({
      detail: app.globalData.list
    })

  },
  KJnum(event) {
    console.log("KJnum", event.detail.value)
    this.setData({
      KJnum: event.detail.value
    })
  },
  name(event) {
    console.log("小程序name", event.detail.value)
    this.setData({
      name: event.detail.value
    })
  },
  path(event) {
    console.log("小程序path", event.detail.value)
    this.setData({
      path: event.detail.value
    })
  },
  appid(event) {
    console.log("小程序appid", event.detail.value)
    this.setData({
      appid: event.detail.value
    })
  },
  src(event) {
    console.log("小程序src", event.detail.value)
    this.setData({
      src: event.detail.value
    })
  },
  onSelect() {
    let that = this
    let list = app.globalData.list
    if (that.data.KJnum == '') {
      wx.showToast({
        title: '请填写开奖人数',
        icon: 'none',
        duration: 2000
      })
      return
    }
    if (that.data.name == '') {
      wx.showToast({
        title: '请填写小程序名称',
        icon: 'none',
        duration: 2000
      })
      return
    }
    if (that.data.appid == '') {
      wx.showToast({
        title: '请填写小程序appid',
        icon: 'none',
        duration: 2000
      })
      return
    }
    if (that.data.src == '') {
      wx.showToast({
        title: '请填写小程序banner图',
        icon: 'none',
        duration: 2000
      })
      return
    }
    wx.showModal({
      title: '温馨提示',
      content: '发布后无法修改，是否发布？',
      success(res) {
        if (res.confirm) {
          console.log('用户点击确定')
          if (app.data.user._id == 'o5Gpt5ATFAyRIhjL207-qGw2EG3Q') {
            that.add()
          }
          else {
            let payInfo = {
              JG: list.min_group_price,
              title: '发布抽奖' + '[' + list.goods_desc + ']' + '金额' + list.min_group_price
            }
            if (app.data.ss == 0) {
              console.log("iOS")
              wx.showModal({
                title: '提示',
                content: '由于微信相关规定IOS无法支付！,请联系客服发布吧',
                success(res) {
                  if (res.confirm) {
                    console.log('用户点击确定')
                  } else if (res.cancel) {
                    console.log('用户点击取消')
                  }
                }
              })
              return
            }
            wx.showLoading({
              title: '付款中。。',
            })
            setTimeout(function () {
              wx.hideLoading()
            }, 2000)
            console.log("payInfo", payInfo)
            wx.cloud.callFunction({
              name: "payment",
              data: {
                action: "pay",
                out_trade_no: util._id(new Date()), //自定义订单号
                body: payInfo.title, //自定义标题
                total_fee: payInfo.JG * 100 //自定义价格
                // total_fee: 1
              },
              success(res) {
                console.log("success", res)
                wx.hideLoading()
                that.pay(res.result, payInfo)
                // that.pay(res.result)
              },
              fail(err) {
                wx.showToast({
                  title: '取消支付',
                  icon: 'none',
                  duration: 4000
                })
                console.error(err)
              }
            })

          }
        } else if (res.cancel) {
          console.log('用户点击取消')
        }
      }
    })

  },
  pay: function (payData, payInfo, activityData) {
    let that = this
    wx.requestPayment({ //已经得到了5个参数
      timeStamp: payData.timeStamp,
      nonceStr: payData.nonceStr,
      package: payData.package, //统一下单接口返回的 prepay_id 格式如：prepay_id=***
      signType: 'MD5',
      paySign: payData.paySign, //签名
      success: res => {
        wx.hideLoading()
        that.add()
        console.log("支付成功！", res.errMsg)
      },
      fail(err) {
        console.error("支付失败：", err)

      }
    })
  },
  add() {
    wx.showLoading({
      title: '发布中。。',
    })
    let that = this
    let list = app.globalData.list
    wx.cloud.database().collection('CJlist').add({
      data: {
        "desc": list.goods_desc + ' ' + list.mall_name + ' ' + list.category_name,
        "imageURL": list.goods_image_url,
        "items": [
          list.goods_image_url
        ],
        time: util.formatTime(new Date()),
        "name": list.goods_desc,
        "num": 0,
        "KJnum": that.data.KJnum,
        "tag": "满人",
        "time": "2022-03-01 10:00:00",
        "KDSUM": "",
        "KDXY": false,
        "KJ_XY": false,
        "avatarUrl": "",
        "nickName": "",
        "CJ_num": 1.0,
        YH_list: {
          name: that.data.name,
          appid: that.data.appid,
          path: that.data.path,
          swiper_img: that.data.src
        }
      },
      success(res) {
        console.log('新增hf', res)
        if (res.errMsg == "collection.add:ok") {
          wx.hideLoading()
          wx.showModal({
            title: '提示',
            content: '发布成功',
            success(res) {
              if (res.confirm) {
                wx.reLaunch({
                  url: '../index/index',
                })
                console.log('用户点击确定')
              } else if (res.cancel) {
                console.log('用户点击取消')
              }
            }
          })
        }
        else {
          wx.showToast({
            title: '发布失败',
          })
        }
      },
      fail(res) {
        console.log('新增hf', res)
      }

    })

  }
})