
var time = null; //setTimeout的ID，用clearTimeout清除
const app = getApp();
const db = wx.cloud.database()
const _ = db.command
const innerAudioContext = wx.createInnerAudioContext()
Page({
    data: {
        box: [],
        TGnum: 0,
        guize: [],
        isauthorize: true,
        hasUserInfo: true,
        luckynum: "", //当前运动到的位置，在界面渲染
        howManyNum: "", //抽奖的剩余次数
        content: {
            index: 0, //当前转动到哪个位置，起点位置
            count: 0, //总共有多少个位置
            speed: 50, //初始转动速度
            cycle: 6 * 12, //转动基本次数：即至少需要转动多少次再进入抽奖环节，这里设置的是转动三次后进入抽奖环节
        },
        prize: 1, //中奖的位置
        luckytapif: true //判断现在是否可以点击
    },

    //点击抽奖
    luckyTap: function () {
        if (app.data.system.ZFXY) {
            var
                that = this,
                howManyNum = that.data.howManyNum, //剩余的抽奖次数
                luckytapif = that.data.luckytapif, //获取现在处于的状态
                prize = Math.floor(Math.random() * 12); //中奖序号,随机生成
            if (luckytapif && howManyNum > 100) { //当没有处于抽奖状态且剩余的抽奖次数大于零，则可以进行抽奖
                console.log('prize:' + prize);
                that.data.content.count = that.data.box.length;
                that.setData({
                    howManyNum: howManyNum - 100 //更新抽奖次数
                });
                that.data.prize = prize; //中奖的序号
                that.data.luckytapif = false; //设置为抽奖状态
                that.roll(); //运行抽奖函数
                that.bgyy()
            } else if (howManyNum < 100 && luckytapif) {
                wx.showModal({
                    title: '',
                    content: '幸运币不足100个',
                    showCancel: false
                })
            }
        } else {
            wx.showModal({
                title: '',
                content: '升级中',
                showCancel: false
            })
        }
    },
    //抽奖
    roll: function () {
        var content = this.data.content,
            prize = this.data.prize, //中奖序号
            that = this;
        if (content.cycle - (content.count - prize) > 0) { //最后一轮的时间进行抽奖
            content.index++;
            content.cycle--;
            that.setData({
                luckynum: content.index % 12 //当前应该反映在界面上的位置
            });
            setTimeout(that.roll, content.speed); //继续运行抽奖函数
        } else {
            if (content.index < (content.count * 3 + prize)) { //判断是否停止

                content.index++;
                content.speed += (550 / 12); //最后一轮的速度，匀加速，最后停下时的速度为550+50
                that.data.content = content;
                that.setData({
                    luckynum: content.index % 12
                });

                setTimeout(that.roll, content.speed);
            } else {
                db.collection('user').doc(that.data._openid).update({
                    data: {
                        // 表示指示数据库将字段自增 10
                        XYB: _.inc(-100)
                    },
                    success: function (res) {
                        if (res.stats.updated == 1) {
                            that.onShow()
                        }
                    }
                })
                //完成抽奖，初始化数据
                console.log('完成');
                content.index = 0;
                content.cycle = 3 * 12;
                content.speed = 50;
                that.data.luckytapif = true;
                clearTimeout(time);
                that.setData({
                    desc: that.data.box[prize].prize,
                    image: that.data.box[prize].image,
                    isluckWindowShow: true,
                })
                console.log('_id', that.data.box[prize]._id)
                that.setData({
                    _id: that.data.box[prize]._id
                })
                wx.cloud.database() //普通获取数据------------
                    .collection("turntable_prize")
                    .where({
                        _id: that.data.box[prize]._id
                    })
                    .get()
                    .then(res => {
                        that.setData({
                            sort: res.data[0].sort,
                            num: res.data[0].num
                        })
                        if (res.data[0].sort > 0) {
                            if (that.data.user.ZP_XY == true) {
                                that.setData({
                                    XYB: 0
                                })
                            } else {
                                that.setData({
                                    XYB: 10
                                })
                            }
                            wx.showModal({
                                title: '抽奖结果',
                                content: that.data.box[prize].prize,
                                success(res) {
                                    if (res.confirm) {
                                        console.log('用户点击确定')
                                        let sum = parseFloat(that.data.box[prize].num.toFixed(1))
                                        db.collection('user').doc(that.data.user._id).update({
                                            data: {
                                                // 表示指示数据库将字段自增 10
                                                sum: _.inc(sum),
                                                ZP_XY: true,
                                                XYB: that.data.XYB
                                            },
                                            success: function (res) {
                                                console.log(res)
                                                if (res.stats.updated == 1) {
                                                    wx.showToast({
                                                        title: '已存入余额',
                                                        icon: 'success',
                                                        duration: 2000
                                                    })
                                                }
                                            }
                                        })
                                    } else if (res.cancel) {
                                        wx.showToast({
                                            title: '您取消了',
                                            icon: 'none',
                                            duration: 4000
                                        })
                                        console.log('用户点击取消')
                                    }
                                }
                            })

                        } else {
                            wx.showModal({
                                title: '',
                                content: '已领完',
                                showCancel: false,
                            })
                        }
                    })

            }
        }
    },
    bindRule() {
        this.setData({
            ruleWindow: true
        })
    },
    bindClose() {
        this.setData({
            ruleWindow: ""
        })
    },
    prizelist() {
        wx.cloud.database() //普通获取数据------------
            .collection("turntable_prize")
            .get()
            .then(res => {
                console.log('获取成功', res.data)
                this.setData({
                    prizelist: res.data
                })
                wx.hideLoading()
            })
    },
    //=============================================
    onLoad: function (e) {
        var a = this,
            s = [];
        for (var i = 1; i <= 30; i++) s.push(i);
        console.log(e), e && e.only && a.setData({
            only: e.only
        }), a.setData({
            marqueeList: s,
        }), a.dataFun()
        a.prizelist()
    },
    bgyy() {
        app.CP()
        const backgroundAudioManager = wx.getBackgroundAudioManager()
        backgroundAudioManager.title = '开始抽奖了';
        backgroundAudioManager.epname = '开始抽奖了';
        backgroundAudioManager.singer = '开始抽奖了';
        backgroundAudioManager.coverImgUrl = '/images/libao.png';
        // 设置了 src 之后会自动播放
        this.onPlay()

    },
    onPlay: function () {
        innerAudioContext.autoplay = true
        innerAudioContext.src = 'cloud://fxt-8gtjwl1u5806d421.6678-fxt-8gtjwl1u5806d421-1306502144/0Axt/yy.mp3',
            innerAudioContext.onPlay(() => {
                console.log('开始播放')
            })
        innerAudioContext.onEnded(() => {
            innerAudioContext.stop()
            console.log('播放完成')
        })
    },
    loginFun: function () {
        this.setData({
            isauthorize: !0,
            hasUserInfo: !0
        })
    },
    dataFun: function () {
        wx.cloud.database()
            .collection("turntable_prize")
            .get()
            .then(res => {
                this.setData({
                    box: res.data,
                    text: app.data.system.text
                })
                wx.hideLoading()
            })
    },
    onShow() {
        wx.cloud.callFunction({ //获取openid------------
            name: 'api',
            data: {
                if: "user",
            }
        }).then(res => {
            console.log("user", res.result.data[0])
            if (res.result.data.length == 1) {
                this.setData({
                    howManyNum: res.result.data[0].XYB,
                    _openid: res.result.data[0]._id,
                    user: res.result.data[0]
                })
            } else {
                wx.showModal({
                    title: '温馨提示',
                    content: '欢迎使用，请登陆哦！',
                    success(res) {
                        if (res.confirm) {
                            console.log('用户点击确定')
                            wx.navigateTo({
                                url: '/pages/denglu/index',
                            })
                        } else if (res.cancel) {
                            console.log('用户点击取消')
                        }
                    }
                })
            }
        })
    },
});