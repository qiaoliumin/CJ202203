const app = getApp();
const db = wx.cloud.database()
const _ = db.command
Page({

  /**
   * 页面的初始数据
   */
  data: {
    activeNames: [],
    CJZT: true,
    CJZTname: "参与抽奖",
    showShare: false,
    options: [
      { name: '微信好友', icon: 'wechat', openType: 'share' },
      { name: '二维码', icon: 'https://6678-fxt-8gtjwl1u5806d421-1306502144.tcb.qcloud.la/m.png?sign=8a3381774fadfd2ee1a329976cd9e0bc&t=1646636670' },
      { name: '公众号', icon: 'https://6678-fxt-8gtjwl1u5806d421-1306502144.tcb.qcloud.la/gzh.png?sign=8348a478b7a0c02e1663137c72006417&t=1646636690' },
      { name: '微信好友', icon: 'wechat', openType: 'share' },

    ],
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let that = this
    wx.showLoading({
      title: '',
    })
    that.setData({
      _id: options.scene,
      path: 'pages/draw/mrkj?scene=' + options.scene
    })
    that.SJ()
  },
  SJ() {
    let that = this
    wx.cloud.database() //普通获取数据------------
      .collection("CJlist")
      .where({
        _id: that.data._id
      })
      .get()
      .then(res => {
        if (res.data.length == 1) {
          wx.hideLoading()
          console.log('抽奖活动详情', res.data[0])
          that.setData({
            _id: that.data._id,
            list: res.data[0],
            zj_list: res.data[0].zj_list,
            num: res.data[0].num,
            KJ_XY: res.data[0].KJ_XY
          })
          that.ZANZHU()
          that.CJLIST()
          that.CJ_user()
          if (res.data[0].KJ_XY) {
            wx.showModal({
              title: '已开奖',
              content: '幸运用户' + res.data[0].nickName,
              success(res) {
                if (res.confirm) {
                  console.log('用户点击确定')
                } else if (res.cancel) {
                  wx.navigateTo({
                    url: '../index/index',
                  })
                  console.log('用户点击取消')
                }
              }
            })
          }
        } else {
          wx.hideLoading()
          wx.showToast({
            title: '抽奖活动已下架',
            icon: 'none',
            duration: 6000
          })
        }
      })
  },
  onShow() {

  },
  ZANZHU() {
    let that = this
    if (that.data.list.YH_list) {
      that.setData({
        ZANZHU: that.data.list.YH_list
      })
    } else {
      db.collection('list').aggregate()
        .match({
          SH: true,
          ZZ: true
        })
        .sample({
          size: 1
        }).end().then(res => {
          console.log(res)
          that.setData({
            ZANZHU: res.list[0]
          })
        })
    }
  },
  type() {
    let that = this
    let typelist = that.data.ZANZHU
    if (typelist.type) {
      if (typelist.type == "xcx") {
        wx.navigateToMiniProgram({
          appId: typelist.appid,
          path: typelist.path,
          envVersion: 'release',// 打开正式版
          success(res) {
            console.log("// 打开成功", res)
            if (res.errMsg == "navigateToMiniProgram:ok") {
              that.setData({
                type_appid: typelist.appid
              })
              wx.cloud.callFunction({ //获取openid------------
                name: 'api',
                data: {
                  if: "list_inc",
                  _id: typelist._id
                }
              }).then(res => {
                console.log("res", res)
              })
            }
          },
          fail: function (err) {
            console.log(err);
          }
        })
      }
      if (typelist.type == "web") {
        wx.navigateTo({
          url: '../wed/index?web=' + typelist.web,
        })
      }
      if (typelist.type == "ma") {
        wx.previewImage({
          urls: [typelist.ma_img],
          current: typelist.ma_img // 当前显示图片的http链接      
        })
      }
    } else {
      wx.navigateToMiniProgram({
        appId: typelist.appid,
        path: typelist.path,
        envVersion: 'release',// 打开正式版
        success(res) {
          console.log("// 打开成功", res)
          if (res.errMsg == "navigateToMiniProgram:ok") {
            that.setData({
              type_appid: typelist.appid
            })
          }
        },
        fail: function (err) {
          console.log(err);
        }
      })
    }
  },
  CJLIST() {
    console.log('获取参与用户')
    wx.cloud.database()
      .collection("CJ")
      .where({
        XY_id: this.data._id
      })
      .get()
      .then(res => {
        console.log('参与用户', res)
        this.setData({
          CJlist: res.data
        })
        wx.hideLoading()
      })
  },
  CJ_user() {
    let that = this
    if (app.data.user) {
      wx.cloud.database()
        .collection("CJ")
        .where({
          _id: that.data.list._id + app.data.user._id,
        })
        .get()
        .then(res => {
          console.log('CJ_user', res.data.length)
          if (res.data.length == 1) {
            that.setData({
              CJZT: false,
              CJZTname: "等待开奖"
            })
          }
        })
    }
    app.CP()
  },
  onChange(event) {
    this.setData({
      activeNames: event.detail,
    });
  },
  CJ() {
    let that = this
    if (that.data.CJZT) {
      if (that.data.list.KJ_XY) {
        console.log("已开奖")
        return
      }
      if (app.data.user == undefined) {
        wx.showModal({
          title: '温馨提示',
          content: '欢迎使用，请登陆哦!',
          success(res) {
            if (res.confirm) {
              console.log('用户点击确定')
              wx.navigateTo({
                url: '/pages/denglu/index',
              })
            } else if (res.cancel) {
              console.log('用户点击取消')
            }
          }
        })
        return
      }
      wx.requestSubscribeMessage({
        tmplIds: [
          '_Xe7leMKrkGjYJgWNT1qaTK-04fYkD5PIxolCfJN_HI'
        ],
        success(res) {
          if (app.data.system.JLXY) {
            wx.showModal({
              title: '提示',
              content: '亲，观看广告后参与抽奖！',
              success(res) {
                if (res.confirm) {
                  console.log('用户点击确定')
                  wx.showLoading({
                    title: '加载中',
                    })
                    
                    setTimeout(function () {
                    wx.hideLoading()
                    }, 2000)
                  // 在页面中定义激励视频广告
                  let videoAd = null
                  // 在页面onLoad回调事件中创建激励视频广告实例
                  if (wx.createRewardedVideoAd) {
                    videoAd = wx.createRewardedVideoAd({
                      adUnitId: 'adunit-8461becb4e7527d3'
                    })
                    videoAd.onClose((status) => {
                      console.log("status")
                      if (status && status.isEnded || status === undefined) {
                        console.log("正常播放")
                        that.CJS()
                      } else {
                        // 播放中途退出，进行提示
                        console.log("中途退出")
                        wx.showToast({
                          icon: 'none',
                          title: '看完广告参与抽奖呢。',
                        })
                      }
                    })
                    videoAd.onError((err) => {
                      console.log("激励视频加载失败")
                    })
                  }

                  // 用户触发广告后，显示激励视频广告
                  if (videoAd) {
                    videoAd.show().catch(() => {
                      // 失败重试
                      videoAd.load()
                        .then(() => videoAd.show())
                        .catch(err => {
                          console.log('激励视频 广告显示失败')
                        })
                    })
                  }
                } else if (res.cancel) {
                  console.log('用户点击取消')
                }
              }
            })
          }
          else {
            that.CJS()
          }
        }
      })
    }
  },
  CJS() {
    let that = this
    wx.cloud.database().collection('CJ').add({
      data: {
        XY: false,
        KJ_XY: false,
        tag: that.data.list.tag,
        _id: that.data.list._id + app.data.user._id,
        XY_id: that.data.list._id,
        avatarUrl: app.data.user.avatarUrl,
        nickName: app.data.user.nickName,
      },
      success() {
        wx.cloud.callFunction({
          name: 'api',
          data: {
            if: "CJlist_JJ",
            _id: that.data.list._id
          }
        }).then(res => {
          console.log("CJlist_JJ", res.result)
          wx.showToast({
            title: '参与成功',
            icon: 'success',
            duration: 10000
          })
          that.SJ()
        })

      },
    })
  },
  SY() {
    wx.reLaunch({
      url: '/pages/index/index',
    })
  },
  FB() {
    wx.reLaunch({
      url: '../pdd/home',
    })
  },
  onClick(event) {
    this.setData({ showShare: true });
  },

  onClose() {
    this.setData({ showShare: false });
  },

  hideModal(e) {
    this.setData({
      modalName: null
    })
  },
  onSelect(event) {
    console.log(event.detail.name)
    this.onClose();
    if (event.detail.name == '二维码') {
      this.WM()
    }
    if (event.detail.name == '海报') {
      this.HBB()
    }
    if (event.detail.name == '公众号') {
      this.GZH()
    }
  },
  WM() {
    wx.showLoading({
      title: '生成中',
    })
    wx.cloud.callFunction({ //获取openid------------
      name: 'MA',
      data: {
        _id: this.data.list._id
      }
    }).then(res => {
      console.log(res.result)
      if (res.result) {
        wx.hideLoading();
        this.setData({
          img: res.result,
          modalName: "Image"
        });
      }
    })
  },
  bc() {
    wx.showLoading({
      title: '保存中',
    })
    let that = this
    console.log(that.data.img)
    wx.cloud.getTempFileURL({
      fileList: [that.data.img],
      success: res => {
        console.log("https--url", res.fileList[0].tempFileURL)
        wx.downloadFile({
          url: res.fileList[0].tempFileURL,
          success(res) {
            console.log(res)
            if (res.statusCode === 200) {
              wx.saveImageToPhotosAlbum({
                filePath: res.tempFilePath,
                success: (res) => {
                  console.log(res)
                  wx.hideLoading();
                  wx.showToast({
                    title: '保存到相册成功',
                  });
                  that.setData({
                    modalName: null
                  })
                },
                fail: (res) => {
                  wx.hideLoading();
                  wx.showToast({
                    title: '保存失败',
                    icon: 'none',
                  });
                }
              })
            } else {
              wx.showToast({
                title: '请重试',
                icon: 'none',
                duration: 2000
              })
            }
          }
        })
      },
      fail: console.error
    })

  },

  GZH() {
    console.log("公众号")
    this.setData({
      modalName: "Modal"
    });
  },
  CopyLink(e) {
    wx.setClipboardData({
      data: "小程序 appid: wxcba5adf71bda7584  " + '小程序路径 path：' + this.data.path,
      success: res => {
        wx.showToast({
          title: '已复制',
          duration: 1000,
        })
        this.setData({
          modalName: ""
        })
      }
    })
  },
  ANNN() {
    wx.showShareMenu({
      withShareTicket: true,
      menus: ["shareAppMessage", "shareTimeline"]
    })
  },
  onShareAppMessage() {
    let list = this.data.list
    console.log(list)
    return {
      title: app.data.user.nickName + '邀你一起，抽👉' + list.name,
      imageUrl: list.imageURL,
      path: '/pages/draw/mrkj?scene=' + list._id + '&TG=' + app.data.user._id
    };
  },
  onShareTimeline() {
    let list = this.data.list
    console.log(list)
    return {
      title: '邀你一起抽👉' + list.name,
      imageUrl: list.imageURL,
      path: '/pages/draw/mrkj?scene=' + list._id + '&TG=' + app.data.user._id
    };
  }
})