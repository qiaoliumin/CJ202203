let totalNum = -1
const app = getApp();
Page({

    /**
     * 页面的初始数据
     */
    data: {
        list: [],
        XL: true
    },
    onLoad(e) {
        this.setData({
            XY_id: e.id,
        })
        wx.cloud.database().collection('CJ')
            .where({
                XY_id: e.id
            })
            .count()
            .then(res => {
                console.log("数据总条数", res)
                totalNum = res.total
            })
        this.getDataList()
    },
    //加载数据
    getDataList() {
        wx.showLoading({
            title: '加载中',
        })
        let that = this
        let len = that.data.list.length
        if (totalNum == len) {
            console.log("全部在这啦")
            that.setData({
                XL: false
            })
            wx.hideLoading()
            return
        }
        
        wx.cloud.database().collection('CJ')
            .where({
                XY_id: that.data.XY_id
            })
            .orderBy('_id', 'desc')
            .skip(len)
            .limit(20)
            .get()
            .then(res => {
                that.setData({
                    list: that.data.list.concat(res.data)
                })
                wx.stopPullDownRefresh()
                wx.hideLoading()
            })
            .catch(res => {
                console.log('获取失败', res)
                wx.hideLoading()
                wx.showToast({
                    title: '加载失败',
                })
            })
    },
    /**
 * 页面相关事件处理函数--监听用户下拉动作
 */
    onPullDownRefresh: function () {
        console.log('监听用户下拉动作')
        this.getDataList()

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {
        console.log('页面上拉触底事件的处理函数')
        this.getDataList()

    },
    onShow() {
        
    },
   
})