const app = getApp();
const db = wx.cloud.database()
const _ = db.command
var util = require('../../utils/util.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    SCXY: true,
    TGnum: 0,
    adds: "中国",
    nickName: "点击登陆",
    avatarUrl: "/icons/c.png",
    show: false,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  },
  onShow() {
    wx.cloud.callFunction({ //获取openid------------
      name: 'api',
      data: {
        if: "user",
      }
    }).then(res => {
      if (res.result.data.length == 1) {
        this.setData({
          sum: res.result.data[0].sum.toFixed(2),
          adds: res.result.data[0].adds,
          nickName: res.result.data[0].nickName,
          avatarUrl: res.result.data[0].avatarUrl,
          user: res.result.data[0]
        })
      }
      else {
        wx.showModal({
          title: '温馨提示',
          content: '欢迎使用，请登陆哦！',
          success(res) {
            if (res.confirm) {
              console.log('用户点击确定')
              wx.navigateTo({
                url: '/pages/denglu/index',
              })
            } else if (res.cancel) {
              console.log('用户点击取消')
            }
          }
        })
      }
    })
  },
  wx() {
    wx.previewImage({
      urls: [app.data.system.wx],
      current: app.data.system.wx // 当前显示图片的http链接      
    })
  },
  dz() {
    let that = this
    wx.chooseAddress({
      success(res) {
        db.collection('user').doc(that.data.user._id).update({
          data: {
            // 表示指示数据库将字段自增 10
            adds: res.userName + '︱' + res.provinceName + res.cityName + res.countyName + res.detailInfo + '︱' + res.telNumber
          },
          success: function (res) {
            if (res.stats.updated == 1) {
              wx.showToast({
                title: '更新成功',
                icon: 'success',
                duration: 6000
              })
              that.onShow()
            }
          }
        })
      }
    })
  },
  CY() {
    wx.navigateTo({
      url: '/pages/me/CY/index?_id=' + this.data.user._id,
    })
  },
  SC() {
    console.log("SC")
    this.setData({
      SCXY: ""
    })
  },
  Admin() {
    if (this.data.user._id == 'o5Gpt5ATFAyRIhjL207-qGw2EG3Q') {
      wx.navigateTo({
        url: '../Admin/index/index?op=o5Gpt5ATFAyRIhjL207-qGw2EG3Q',
      })
    }

  }
})