let totalNum = -1
const app = getApp();
Page({
    /**
     * 页面的初始数据
     */
    data: {
        list: []
    },
    onLoad(e) {
        if (e._id) {
            this.setData({
                _id: e._id
            })
            wx.cloud.database().collection('CJ')
                .where({
                    _openid: e._id
                })
                .count()
                .then(res => {
                    console.log("数据总条数", res)
                    totalNum = res.total
                })
            this.getDataList()
        }

    },
    //加载数据
    getDataList() {
        app.CP()
        console.log("加载数据")
        let len = this.data.list.length
        if (totalNum == len) {
            console.log("数据已加载完")
            wx.stopPullDownRefresh()
            return
        }
        wx.showLoading({
            title: '加载中',
        })
        wx.cloud.database().collection('CJ')
            .where({
                _openid: this.data._id
            })
            .orderBy('time', 'desc')
            .skip(len)
            .limit(20)
            .get()
            .then(res => {
                this.setData({
                    list: this.data.list.concat(res.data)
                })
                wx.stopPullDownRefresh()
                wx.hideLoading()
            })
            .catch(res => {
                console.log('获取失败', res)
                wx.hideLoading()
                wx.showToast({
                    title: '加载失败',
                })
            })
    },
    /**
 * 页面相关事件处理函数--监听用户下拉动作
 */
    onPullDownRefresh: function () {
        console.log('监听用户下拉动作')
        this.getDataList()
    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {
        console.log('页面上拉触底事件的处理函数')
        this.getDataList()
    },
    draw(e) {
        let _id = this.data.list[e.currentTarget.dataset.index].XY_id
        wx.navigateTo({
            url: '/pages/draw/mrkj?_id=' + _id,
        })
    },
    kd(e) {
        let index = e.currentTarget.dataset.index
        let KDSUM = this.data.list[index].KDSUM
        let KDXY = this.data.list[index].KDXY
        if (KDXY) {
            wx.navigateTo({
                url: '/pages/me/CY/KD?sum=' + KDSUM,
            })
        }

    },
    onShow() {

    },

})