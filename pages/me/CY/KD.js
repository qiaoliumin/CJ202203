const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    status: ["查询异常", "暂无记录", "在途中", "派送中", "已签收", "用户拒签", "疑难件", "无效单", "超时单", "签收失败", "退回"],
    list: [],
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let that = this
    wx.request({
      url: app.data.system.query + '&number=' + options.sum,
      success: function (result) {
        console.log(result)
        if (result.data.msg == "数据返回为空") {
          wx.showModal({
            title: '查询失败',
            content: '查询快递信息失败',
            showCancel: false
          })
        } else if (result.data.msg == "success") {
          console.log(result)
          var list = result.data.newslist[0]
          that.setData({
            list: list
          })
        } else {
          wx.showModal({
            title: '查询失败',
            content: '未知错误',
            showCancel: false
          })
        }
      }

    })

  },
  onShow() {

  },
 
})