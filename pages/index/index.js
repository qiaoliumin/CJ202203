let totalNum = -1
const app = getApp();
const db = wx.cloud.database()
const _ = db.command
var util = require('../../utils/util.js');
Page({

    /**
     * 页面的初始数据
     */
    data: {
        list: [],
        iconList: [{
            icon: 'squarecheck',
            url: "../Sign/index",
            badge: "+15币",
            name: '每日签到'
        }, {
            icon: 'goods',
            url: "../pdd/home",
            badge: 999,
            name: '优惠商城'
        }, {
            url: "../turntable/index",
            icon: 'focus',
            badge: 12,
            name: '幸运转盘'
        }, {
            icon: 'forward',
            badge: 10,
            name: '邀请好友',
            url: "../FXT/home",
        }],
        gridCol: 4,
        skin: false
    },
    onLoad() {
        totalNum = -1
        this.setData({
            list: []
        })
        wx.cloud.database().collection('CJlist')
            .where({
                KJ_XY: false
            })
            .count()
            .then(res => {
                console.log("数据总条数", res)
                totalNum = res.total
            })
        app.CP()
        this.getDataList()
    },
    //加载数据
    getDataList() {
        let len = this.data.list.length
        if (totalNum == len) {
            wx.stopPullDownRefresh()
            return
        }
        wx.showLoading({
            title: '加载中',
        })
        wx.cloud.database().collection('CJlist')
            .where({
                KJ_XY: false
            })
            .orderBy('_createTime', 'desc')
            .skip(len)
            .limit(20)
            .get()
            .then(res => {
                this.setData({
                    list: this.data.list.concat(res.data),
                    Notice: app.data.system.Notice
                })
                wx.stopPullDownRefresh()
                wx.hideLoading()
            })
            .catch(res => {
                console.log('获取失败', res)
                wx.hideLoading()
                wx.showToast({
                    title: '加载失败',
                })
            })
    },

    onShow() {
        let that = this
        wx.cloud.callFunction({
            name: 'api',
            data: {
                if: "user",
            }
        }).then(res => {
            if (res.result.data.length == 1) {
                that.setData({
                    sum: res.result.data[0].sum.toFixed(2),
                    user: res.result.data[0]
                })
                app.data.user = res.result.data[0]
            } else {
                wx.showModal({
                    title: '温馨提示',
                    content: '欢迎使用，请登陆哦！',
                    success(res) {
                        if (res.confirm) {
                            console.log('用户点击确定')
                            wx.navigateTo({
                                url: '/pages/denglu/index',
                            })
                        } else if (res.cancel) {
                            console.log('用户点击取消')
                        }
                    }
                })
            }

        })
    },
    fruit(e) {
        console.log(e.currentTarget.dataset.url)
        wx.navigateTo({
            url: e.currentTarget.dataset.url,
        })
    },
    /**
 * 页面相关事件处理函数--监听用户下拉动作
 */
    onPullDownRefresh: function () {
        console.log('监听用户下拉动作')
        this.getDataList()
    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {
        this.getDataList()
    },
    XJTX() {
        let that = this
        let user = that.data.user
        console.log(user.sum.toFixed(1))
        if (user.sum < 20) {
            wx.showToast({
                title: '满20元起可提现',
                icon: 'none',
                duration: 2000
            })
            return
        }
        wx.showLoading({
            title: '发放中...',
        })
        wx.cloud.callFunction({
            name: 'lq',
            data: {
                type: "transfers",
                openid: user._openid,
                appid: "wxcba5adf71bda7584",///appid
                partner_trade_no: util._id(new Date()),
                amount: user.sum.toFixed(1) * 100,///单位：分
                desc: '分享推小程序 ' + '提现红包' + user.sum + '元'//描述
            }
        }).then(res => {
            console.log(res)
            if (res.result.result_code == 'SUCCESS') {
                wx.showToast({
                    title: '已发放微信零钱',
                    icon: 'success',
                    duration: 2000
                })
                setTimeout(function () {
                    wx.hideLoading()
                }, 200)
                db.collection('user').doc(user._openid).update({
                    data: {
                        // 表示指示数据库将字段自增 10
                        sum: _.inc(-user.sum)
                    },
                    success: function (res) {
                        if (res.stats.updated == 1) {
                            that.onShow()
                        }
                    }
                })
            }
            else {
                wx.showToast({
                    title: '发放失败！',
                    icon: 'success',
                    duration: 2000
                })
                setTimeout(function () {
                    wx.hideLoading()
                }, 200)
            }
        }).catch(err => {
            console.log(err)
        })
    },
    draw(e) {
        let index = e.currentTarget.dataset.index
        let list = this.data.list
        let _id = list[index]._id
        let tag = list[index].tag
        if (tag == '满人') {
            wx.navigateTo({
                url: '/pages/draw/mrkj?scene=' + _id,
            })
        }
        else {
            wx.navigateTo({
                url: '/pages/draw/dskj?scene=' + _id,
            })
        }
    },
    ANNN() {
        wx.showShareMenu({
            withShareTicket: true,
            menus: ["shareAppMessage", "shareTimeline"]
        })
    },
    onShareAppMessage() {
        return {
            title: app.data.system.fx_text,
            imageUrl: app.data.system.fx_img,
            path: '/pages/index/index?scene=' + app.globalData.OP
        };
    },
    onShareTimeline() {
        return {
            title: app.data.system.fx_text,
            imageUrl: app.data.system.fx_img,
            path: '/pages/index/index?scene=' + app.globalData.OP
        };
    }
})