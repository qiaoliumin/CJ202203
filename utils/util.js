const formatTime = date => {
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()
  const hour = date.getHours()
  const minute = date.getMinutes()
  const second = date.getSeconds()

  return [year].map(formatNumber) + "-" + [month].map(formatNumber) + "-" + [day].map(formatNumber) + " " + [hour].map(formatNumber) + ":" + [minute].map(formatNumber) + ":" + [second].map(formatNumber)
}
const _id = date => {
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()
  const hour = date.getHours()
  const minute = date.getMinutes()
  const second = date.getSeconds()
  return [year].map(formatNumber) + [month].map(formatNumber) + [day].map(formatNumber) + [hour].map(formatNumber) + [minute].map(formatNumber) + [second].map(formatNumber)
}

const formatNumber = n => {
  n = n.toString()
  return n[1] ? n : '0' + n
}


module.exports = {
  formatTime: formatTime,
  _id: _id
}