
const cloud = require('wx-server-sdk')

cloud.init({
  env: 'fxt-8gtjwl1u5806d421'
})
const db = cloud.database()
var sendMsg = async (item) => {
  const result = await cloud.openapi.subscribeMessage.send({
    touser: item._id,
    page: 'pages/index/index',
    lang: 'zh_CN',
    data: {
      thing1: {
        value: '签到领幸运币，连续签到领大礼。'
      },
      thing16: {
        value: '您已有幸运 ' + item.XYB + '币个'
      }
    },
    templateId: '7fHKPTQr__G2cz9HWozZtGLqt-8d-4ABjyTJ-4PL8vw'
  })
  const update = await db.collection('user')
    .where({

    })
    .update({
      data: {
        QD_XY: false,  //每日签到
        JLGG_XY: false, //每日看广告签到
        CJXY: false, //每日参加一个抽奖活动,
        ZP_XY: true,//每日参加一个幸运大转盘活动,
      }
    })

}

const MAX_LIMIT = 100
exports.main = async (event, context) => {
  // 先取出集合记录总数
  const countResult = await db.collection('user')
    .where({
      QD_XY: true
    }).count()
  const total = countResult.total
  // 计算需分几次取
  const batchTimes = Math.ceil(total / 100)
  // 承载所有读操作的 promise 的数组
  const tasks = []
  for (let i = 0; i < batchTimes; i++) {
    const promise = db.collection('user').where({
      QD_XY: true
    }).skip(i * MAX_LIMIT).limit(MAX_LIMIT).get()
    tasks.push(promise)
  }
  // 等待所有
  var arr = (await Promise.all(tasks)).reduce((acc, cur) => {
    return {
      data: acc.data.concat(cur.data),
      errMsg: acc.errMsg,
    }
  })
  for (var i = 0; i < arr.data.length; i++) {
    sendMsg(arr.data[i])
  }
  return {
    countResult: countResult,
    arr: arr
  }
}