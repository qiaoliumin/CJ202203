// 云函数入口文件
const cloud = require('wx-server-sdk')
const md5 = require('md5');
const request = require('request-promise');
cloud.init({
  env: cloud.DYNAMIC_CURRENT_ENV
})

const client_id = "c7565b7b63bf4abb99820143d1e113a0";
const clientSecret = "c657f1e309963909b08b104c24b4e0a0c096649b";
const apiUrl = "https://gw-api.pinduoduo.com/api/router";

// 云函数入口函数
exports.main = async (event, context) => {
  console.log(event, '传入数据')
  switch (event.type) {
    case 'pddhot': {
      // 爆款商品
      var params = {
        // type:'pdd.ddk.top.goods.list.query',
        type: 'pdd.ddk.goods.recommend.get',
        client_id: client_id,
        timestamp: new Date().getTime(),
        list_id: event.data.list_id ? event.data.list_id : '',
        offset: event.data.offset,
        limit: event.data.limit,
        sort_type: 1,
      }
      return pdd(params);
    }
    case 'pddurl': {
      // 生成小程序推广链接
      var params = {
        type: 'pdd.ddk.goods.promotion.url.generate',
        client_id: client_id,
        timestamp: new Date().getTime(),
        search_id: event.data.earchid,
        // generate_qq_app:true,
        generate_we_app: true,
        goods_sign: event.data.goods_sign,
        p_id: '15040575_198772197',
      }
      return pdd(params);
    }
    case 'pddsearch': {
      // 搜索内容
      var params = {
        type: 'pdd.ddk.goods.search',
        client_id: client_id,
        timestamp: new Date().getTime(),
        keyword: event.data.keyword,
        page: event.data.page,
        page_size: event.data.page_size,
        with_coupon: true,
        pid: '15040575_198772197',
      }
      return pdd(params);

    }
    default: {
      return
    }
  }
}

// 拼多多封装请求
async function pdd(params) {
  try {
    let requestUrl = `${apiUrl}?`;
    const keyArray = [];
    Object.keys(params).forEach((key) => {
      keyArray.push(key);
      requestUrl = requestUrl.concat(`${key}=${params[key]}&`);
    });
    const orderedKeyArray = keyArray.sort();
    let sign = clientSecret;
    orderedKeyArray.forEach((key) => {
      sign = sign.concat(`${key}${params[key]}`);
    });
    sign = sign.concat(clientSecret);
    const md5Sign = md5(sign);
    const capitalMd5Sign = md5Sign.toUpperCase();
    requestUrl = encodeURI(requestUrl.concat(`sign=${capitalMd5Sign}`));
    console.log(requestUrl, '11111111')

    var options = {
      method: 'POST',
      uri: requestUrl,
      headers: {
        "Content-Type": "application/json"
      }
    };
    return request(options)
      .then(function (res) {
        console.log(res, "qijngqiui")
        return res;
      })
      .catch(function (err) {
        console.log(err, '拼多多请求获取失败');
        return err
      });

  } catch (err) {
    return err;
  }
}