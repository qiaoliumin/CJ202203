// 云函数入口文件
const cloud = require('wx-server-sdk')
cloud.init()
const db = cloud.database()
const _ = db.command
exports.main = async (event, context) => {
  const wxContext = cloud.getWXContext()
  try {
    if (event.if == "CJlist_JJ") {
      return await db.collection('CJlist').doc(event._id).update({
        data: {
          num: _.inc(1)
        }
      })
    }
    //体验小程序自增
    if (event.if == "list_inc") {
      return await db.collection('list').doc(event._id).update({
        data: {
          sum: _.inc(1),
          T_sum: _.inc(-1)
        }
      })
    }
    //登陆写入
    if (event.if == "add_user") {
      return await cloud.database().collection('user').add({
        data: {
          _id: wxContext.OPENID,
          nickName: event.nickName,
          avatarUrl: event.avatarUrl,
          TG: event.tg_openid,
          TGnum: 0,
          sum: 3.2,
          time: "2022-02-13 21:02:35",
          QD_sum: 0,//初始签到天数
          QD_XY: false,//初始签到状态
          XYB: 88,//初始幸运币
          adds: event.adds,
          _openid: wxContext.OPENID,
          HY_XY: false, //好友邀请
          dz_XY: false,//地址更新
          CJXY: false,//每日一个抽奖
          JLGG_XY: false,//每日观看广告
          ZP_XY: false,//每日转盘
        }
      })
    }
    //XYB 任务更新
    if (event.if == "TGADD") {
      return await db.collection('user').doc(event.tg_openid).update({
        data: {
          HY_XY: event.HY_XY,
          XYB: _.inc(event.XYB)
        }
      })
    }
    if (event.if == "user") {
      return await cloud.database()
        .collection("user")
        .where({
          _id: wxContext.OPENID,
        })
        .get()
    }


    //签到更新写入
    if (event.if == "QD") {
      return await db.collection('user').doc(event._id).update({
        data: {
          QD_XY: true,
          QD_sum: _.inc(1),
          XYB: _.inc(event.XYB)
        }
      })
    }
    //签到更新为0
    if (event.if == "QD_0") {
      return await db.collection('user').doc(event._id).update({
        data: {
          QD_XY: true,
          QD_sum: 0,
          XYB: _.inc(100)
        }
      })
    }
  } catch (e) {
    console.error(e)

  }
  return {
    event,
    openid: wxContext.OPENID,
    appid: wxContext.APPID,
    unionid: wxContext.UNIONID,
  }
}