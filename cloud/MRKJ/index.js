// 云函数入口文件
const cloud = require('wx-server-sdk')
cloud.init()
const db = cloud.database()
// 云函数入口函数
var sendMsg = async (item) => {
    const result = await cloud.openapi.subscribeMessage.send({
        touser: item._openid,
        page: '/pages/draw/mrkj?scene=' + item.XY_id,
        lang: 'zh_CN',
        data: {
            thing1: {
                value: '您参加的抽奖活动已开奖'
            },
            thing2: {
                value: item.list.name,
            },
            thing5: {
                value: '点击查看抽奖详情'
            }
        },
        templateId: '_Xe7leMKrkGjYJgWNT1qaTK-04fYkD5PIxolCfJN_HI',
    })
}

const MAX_LIMIT = 100
exports.main = async (event, context) => {
    //获取全部该活动用户数量
    const countResult = await db.collection('CJ')
        .where({
            XY_id: event._id
        })
        .count()
    //随机抽取该活动一名用户
    const CJlist = await cloud.database().collection('CJ')
        .aggregate()
        .match({
            XY_id: event._id
        })
        .sample({
            size: 1
        })
        .end()
    //活动数据表写入中奖用户
    const addlist = await db.collection('CJlist').doc(CJlist.list[0].XY_id).update({
        data: {
            KJ_XY: true,//活动数据表开奖状态
            avatarUrl: CJlist.list[0].avatarUrl,
            nickName: CJlist.list[0].nickName,
            CJXY_id: CJlist.list[0]._openid
        }
    })
    //参与活动数据表更新中奖用户数据
    const updateS = await db.collection('CJ').doc(CJlist.list[0]._id).update({
        data: {
            XY_id: CJlist.list[0].XY_id,//活动ID
            XY: true,//中奖状态
            KJ_XY: true,//活动状态
            KDXY: false,//默认快递未发货
            KDSUM: ""//空快递单号
        }
    })
    //参与活动数据表更新未中奖状态
    const so_CJ = await db.collection('CJ')
        .where({
            XY_id: CJlist.list[0].XY_id,
        })
        .update({
            data: {
                KJ_XY: true,//活动状态
            }
        })
    const total = countResult.total
    // 计算需分几次取
    const batchTimes = Math.ceil(total / 100)
    // 承载所有读操作的 promise 的数组
    const tasks = []
    for (let i = 0; i < batchTimes; i++) {
        const promise = db.collection('CJ')
            .where({
                XY_id: event._id
            })
            .skip(i * MAX_LIMIT).limit(MAX_LIMIT).get()
        tasks.push(promise)
    }

    // 等待所有
    var arr = (await Promise.all(tasks)).reduce((acc, cur) => {
        return {
            data: acc.data.concat(cur.data),
            errMsg: acc.errMsg,
        }
    })
    for (var i = 0; i < arr.data.length; i++) {
        sendMsg(arr.data[i])
    }
    return {
        event,
        countResult: countResult.total,
        CJlist: CJlist.list[0],
        addlist: addlist,
        updateS: updateS,
        so_CJ: so_CJ
    }
}