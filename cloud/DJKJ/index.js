function processDate(date) {
  date = date.toLocaleString("zh-cn", { timeZone: "Asia/Shanghai" });
  date = new Date(date);
  var y = date.getFullYear();
  var m = date.getMonth() + 1;
  m = m < 10 ? ('0' + m) : m;
  var d = date.getDate();
  d = d < 10 ? ('0' + d) : d;
  var h = date.getHours();
  h = h < 10 ? ('0' + h) : h;
  var minute = date.getMinutes();
  minute = minute < 10 ? ('0' + minute) : minute;
  var second = date.getSeconds();
  second = second < 10 ? ('0' + second) : second;
  // return y + '-' + m + '-' + d + ' ' + h + ':' + minute + ':' + second; 日期
  return y + m + d + h + minute + second; //日期
};
function processDates(date) {
  date = date.toLocaleString("zh-cn", { timeZone: "Asia/Shanghai" });
  date = new Date(date);
  var y = date.getFullYear();
  var m = date.getMonth() + 1;
  m = m < 10 ? ('0' + m) : m;
  var d = date.getDate();
  d = d < 10 ? ('0' + d) : d;
  var h = date.getHours();
  h = h < 10 ? ('0' + h) : h;
  var minute = date.getMinutes();
  minute = minute < 10 ? ('0' + minute) : minute;
  var second = date.getSeconds();
  second = second < 10 ? ('0' + second) : second;
  return y + '-' + m + '-' + d + ' ' + h + ':' + minute + ':' + second;
};
const cloud = require('wx-server-sdk')
cloud.init()
const db = cloud.database()
const _ = db.command
const currentTime = processDate(new Date());
const currentTimes = processDates(new Date());
// 云函数入口函数
var sendMsg = async (item) => {
  const result = await cloud.openapi.subscribeMessage.send({
    touser: item._openid,
    page: '/pages/draw/mrkj?scene=' + item.XY_id,
    lang: 'zh_CN',
    data: {
      thing1: {
        value: '您参加的抽奖活动已开奖'
      },
      thing2: {
        value: item.list.name,
      },
      thing5: {
        value: '点击查看抽奖详情'
      }
    },
    templateId: '_Xe7leMKrkGjYJgWNT1qaTK-04fYkD5PIxolCfJN_HI',
  })
}

const MAX_LIMIT = 100
exports.main = async (event, context) => {


  //获取全部该活动用户数量
  const countResult = await db.collection('CJ')
    .where({
      tag: "定时"
    })
    .count()
  const total = countResult.total
  // 计算需分几次取
  const batchTimes = Math.ceil(total / 100)
  // 承载所有读操作的 promise 的数组
  const tasks = []
  for (let i = 0; i < batchTimes; i++) {
    const promise = db.collection('CJ')
      .where({
        tag: "定时"
      })
      .skip(i * MAX_LIMIT).limit(MAX_LIMIT).get()
    tasks.push(promise)
  }

  // 等待所有
  var arr = (await Promise.all(tasks)).reduce((acc, cur) => {
    return {
      data: acc.data.concat(cur.data),
      errMsg: acc.errMsg,
    }
  })
  for (var i = 0; i < arr.data.length; i++) {
    sendMsg(arr.data[i])
  }
  //随机抽取该活动一名用户
  const CJlist = await db.collection('CJ')
    .aggregate()
    .match({
      KJ_XY: false,
      tag: "定时"
    })
    .sample({
      size: 1
    })
    .end()
  const KJJG = await cloud.callFunction({
    name: 'lq',
    data: {
      type: "transfers",
      openid: CJlist.list[0]._openid,
      appid: "wxcba5adf71bda7584",///appid
      partner_trade_no: currentTime,
      amount: 0.3 * 100,///单位：分
      desc: '分享推小程序 ' + '2分钟定时奖活动' + '0.3元'//描述
    }
  })
  const yh_list = {
    avatarUrl: CJlist.list[0].avatarUrl,
    nickName: CJlist.list[0].nickName,
    time: currentTimes
  }
  //活动数据表写入中奖用户
  const addlist = await db.collection('CJlist').doc(CJlist.list[0].XY_id).update({
    data: {
      zj_list: _.push(yh_list)
    }
  })
  //参与活动数据表更新中奖用户数据
  const remove = await db.collection('CJ')
    .where({
      XY_id: CJlist.list[0].XY_id,
    })
    .remove({
    })


  return {
    event,
    countResult: countResult.total,
    CJlist: CJlist.list[0],
    addlist: addlist,
    arr: arr,
    remove: remove,
    KJJG: KJJG,
  }
}